// ConsoleApplication2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include"shmatrica.h"
#include "shvector.h"
//using namespace std;

int main()
{
	int a = -1;
	for ( /*область инициализации (объявляем переменные для цикла)*/; a != 0; /*повторяемая область*/)  // !=  - не равно
	{
		setlocale(0, "");//установили русский язык
		cout << "Выберите действие:" << endl <<
			"1 Сложение матриц" << endl <<
			"2 Умножение матриц" << endl <<
			"3 Скалярное произведение векторов" << endl <<
			"4 Векторное произведение векторов" << endl <<
			"0-выход" << endl;
		cin >> a;
		switch (a)//оператор выбора (сравнивает значение с значением кейса)
		{
		case 1://если условия совпадают, то заходим в кейс
		{
			schwein a, b, c;
			cin >> a >> b;
			c = a + b;
			cout << c;
			break;//выход из цикла(без него мы будем заходить в другие кейсы
		}
		case 2:
		{
			schwein a, b, c;
			cin >> a >> b;
			c = a*b;
			cout << c;
			break;
		}
		case 3:
		{
			shvector a, b, c;
			cin >> a >> b;
			c.sk(a, b);
			cout << c;
			break;
		}
		case 4:
		{
			shvector a, b, c;
			cin >> a >> b;
			c.vec(a, b);
			cout << c;

			break;
		}
		default://если не сработает ни один из кейсов
			break;
		}
	}
	//system("pause");//остановили программу

    return 0;
}

